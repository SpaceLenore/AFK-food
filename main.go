package main

import (
	"log"
	"net/http"
)

func main() {
	ol := &OrderList{}
	http.Handle("/", http.FileServer(http.Dir("./www")))

	http.HandleFunc("/api/submit", func(w http.ResponseWriter, r *http.Request) {
		submitHandler(w, r, ol)
	})
	http.HandleFunc("/api/remove", func(w http.ResponseWriter, r *http.Request) {
		removeHandler(w, r, ol)
	})
	http.HandleFunc("/api/food", func(w http.ResponseWriter, r *http.Request) {
		foodHandler(w, r, ol)
	})

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	log.Fatal(http.ListenAndServe(":8000", nil))
}
