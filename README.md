# AFK-Food
> This web server might help AFK hackers organise food orders.

## Screenshot
![alt text](Screenshot.png "Screenshot")


## Installation
- Set up a `$GOPATH` if you don't have one already. (`mkdir ~/.go`, `export GOPATH=~/.go`, the latter preferably in your shell .rc)
- `go get gitlab.com/Niesch/AFK-food`
- The binary is now installed in `~/.go/bin/`, and the source is in its workspace.

## Running
- Just run the binary. `./AFK-food` or `AFK-food` if you've modified your `$PATH` to contain `$GOPATH/bin`. The server starts and listens on port 8000.

## Usage
- Visit `http://<ip>:8000` from a browser, of if you're a hardcore 1337 h4xx0r, `curl --data "name=Kaese&contents=Gyrosrulle" http://<ip>:8000/api/submit`.

## Nginx
- If you wanna be leet you can use nginx to proxy the API and serve the static html files.
- Here is a snippet of my Nginx config.
```
location / {
  root   /www;  change this to where you store the static files.
  index index.html index.htm;
  charset UTF-8;
}
location /api {
	proxy_set_header X-Real-IP $remote_addr;
	proxy_set_header X-Forwarded-For $remote_addr;
	proxy_set_header Host $host;
	proxy_pass http://localhost:8000;
}
```

## Security
- There is no authentication of any kind. All data is public. Trust no one.
