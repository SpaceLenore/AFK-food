FROM golang:alpine

RUN apk update && apk add --no-cache git

RUN go get gitlab.com/Niesch/AFK-food

WORKDIR /go/src/gitlab.com/Niesch/AFK-food

CMD ["/go/bin/AFK-food"]
