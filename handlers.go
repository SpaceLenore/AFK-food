package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func submitHandler(w http.ResponseWriter, r *http.Request, ol *OrderList) {
	if r.Method != "POST" {
		fmt.Fprintf(w, "You may only POST to /api/submit. Example payload: `name=foo&contents=bar`.\n")
		return
	}
	r.ParseForm()
	if len(r.Form["name"]) > 0 && len(r.Form["contents"]) > 0 {
		ol.currentID++
		ol.Append(Order{Name: r.Form["name"][0], Contents: r.Form["contents"][0], Date: time.Now().Format(time.RFC822), Id: ol.currentID})
		http.Redirect(w, r, "/", 301)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid request - Order needs both name and contents.\n")
	}
}

func removeHandler(w http.ResponseWriter, r *http.Request, ol *OrderList) {
	if r.Method != "POST" {
		fmt.Fprintf(w, "You may only POST to /api/remove. Example payload: `id=1`.\n")
		return
	}
	r.ParseForm()
	if len(r.Form["id"]) > 0 {
		i, err := strconv.Atoi(r.Form["id"][0])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "I ain't removing that. Send me an ID.\n")
		} else {
			ol.DeleteFromID(i)
			http.Redirect(w, r, "/", 301)
			fmt.Fprintf(w, "OK.\n")
		}
	}
}

func foodHandler(w http.ResponseWriter, r *http.Request, ol *OrderList) {
	if r.Method != "GET" {
		fmt.Fprintf(w, "You may only GET to /api/food.\n")
		return
	}

	j, err := json.Marshal(ol.Orders)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%s", err.Error())
		return
	}
	fmt.Fprintf(w, string(j))
}
